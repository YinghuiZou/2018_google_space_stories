'use strict';
const functions = require('firebase-functions');
const {
  dialogflow,
  Suggestions,
  BasicCard,
  SimpleResponse,
} = require('actions-on-google');

const categoryMsg = 'The categories are: History of Space, Becoming an' +
  ' astronaut, Space Travel, Gravity facts, Walking in space, Life in space,' +
  ' and Cool Facts. Please select a category or say play all to start from' +
  ' the beginning.';
const audioMsg = ['Enjoy!', 'Here it is!', 'Okay!', 'Here!'];




const app = dialogflow();


app.intent('Default Welcome Intent', (conv) => {

  const URLLINK =
  'https://s3.amazonaws.com/2018-google-space-stories/audio/intro/googlehomespaceintro.mp3';
  const OutputUrl = `<audio src="${URLLINK}"/>` +
  ` Would you like to listen to Mikes biography? Say` +
  ` biography. Or say Space Stories to choose a category.`;

  conv.user.storage.play_all_index = 1;
  let speech = getSSMLSpeech(OutputUrl);
    conv.ask(speech);
  });


  app.intent('Author Bio Intent', (conv) => {

    const URLLINK =
    'https://s3.amazonaws.com/2018-alexa-space-stories/audio/intro_and_bio/biowithmusic1.mp3';
    const OutputUrl = `<audio src="${URLLINK}"/>` +
    ` Say space stories to choose a category from space stories,` +
    `  or say learn more to get more information about space.`;
    let speech = getSSMLSpeech(OutputUrl);
      conv.ask(speech);
    });


    

  app.intent('Learn more Intent', (conv) => {

    let speech =
    ` To learn more about Mike and his space stories go to ` +
    `  to, w w w dot mike massimino dot com. Say space story to choose a category from space stories.`;
   
      conv.ask(speech);
    });


    app.intent('Space Stories Intent', (conv) => {

      let message = categoryMsg;
      conv.ask(message);
      });


    
    
    
    
    
    
    
    

          




      app.intent('Next Audio Intent', (conv) => {
        let index;
        let category = conv.user.storage.category;
        let message = 'There is no more topic in this category. Choose' +
        ' another category. To listen to all of the categories, say Space' +
        ' Stories.';

        let suggestionArray = [
          'History of Space',
          'Becoming an Astronaut',
          'Space Travel',
          'Gravity Facts',
          'Walking in Space',
          'Life in Space',
          'Cool Facts',
          'Play All',
        ];

        


        switch (category) {
          case 'history_of_space':
          conv.user.storage.history_of_space_index += 1;
          console.log('The current count for the audio is: ' + conv.user.storage.history_of_space_index );
            index =  conv.user.storage.history_of_space_index;
    
            if (index > 3) {
              conv.user.storage.history_of_space_index = 1;
              conv.ask('There is no more topic in this category');
            }else{

              history_of_Space(conv);

            }
    
           break;



           case 'becoming_an_astronaut':
          conv.user.storage.becoming_an_astronaut_index += 1;
            index =  conv.user.storage.becoming_an_astronaut_index;
    
            if (index > 3) {
              conv.user.storage.becoming_an_astronaut_index = 1;
              conv.ask('There is no more topic in this category');
            }else{

              Become_An_Astronaut(conv);

            }
    
           break; 

           case 'space_travel':
           conv.user.storage.space_travel_index += 1;
             index =  conv.user.storage.space_travel_index;
     
             if (index > 3) {
               conv.user.storage.space_travel_index = 1;
               conv.ask('There is no more topic in this category');
             }else{
 
               Space_Travel(conv);
 
             }
     
            break; 

            case 'gravity_facts':
            conv.user.storage.gravity_facts_index += 1;
              index =  conv.user.storage.gravity_facts_index;
      
              if (index > 2) {
                conv.user.storage.gravity_facts_index = 1;
                conv.ask('There is no more topic in this category');
              }else{
  
                Gravity_Facts(conv);
  
              }
      
             break;


             case 'walking_in_space':
             conv.user.storage.walking_in_space_index += 1;
               index =  conv.user.storage.walking_in_space_index;
       
               if (index > 2) {
                 conv.user.storage.walking_in_space_index = 1;
                 conv.ask('There is no more topic in this category');
               }else{
   
                Walking_In_Space(conv);
   
               }
       
              break;

              case 'life_in_space':
              conv.user.storage.life_in_space_index += 1;
                index =  conv.user.storage.life_in_space_index;
        
                if (index > 6) {
                  conv.user.storage.life_in_space_index = 1;
                  conv.ask('There is no more topic in this category');
                }else{
    
                  Life_In_Space(conv);
    
                }
        
               break;


               case 'cool_facts':
               conv.user.storage.cool_facts_index += 1;
                 index =  conv.user.storage.cool_facts_index;
         
                 if (index > 3) {
                   conv.user.storage.cool_facts_index = 1;
                   conv.ask('There is no more topic in this category');
                 }else{
     
                  Cool_Facts(conv);
     
                 }
         
                break;


                case 'play_all':
               conv.user.storage.play_all_index += 1;
                 index =  conv.user.storage.play_all_index;
         
                 if (index > 6) {
                   conv.user.storage.play_all_index = 1;
                   conv.ask('There is no more topic in this category');
                 }else{
     
                  Play_All(conv);
     
                 }
         
                break;




         }

    
        });
  

        












      app.intent('Play All Intent', (conv) => {
        Play_All(conv);
        
        });























      /**
   * [playAudio description]
   * @param  {[type]} category   [description]
   * @param  {[type]} audioTitle [description]
   * @param  {[type]} index      [description]
   * @return {[type]}            [description]
   */
  function playAudio(category, audioTitle, index, conv) {
    console.log('It goes inside the play all audio');
    console.log('The category and the index is: Category:  ' + category + ' index: ' + index );
    let audioUrl = 'https://s3.amazonaws.com/2018-google-space-stories/audio/' +
      category + '/' + index + '.mp3';
    let iconUrl;
    if (category === 'play_all') {
      let iconCategory;
      switch (index) {
        case 1:
          iconCategory = 'history_of_space';
          break;
        case 2:
          iconCategory = 'becoming_an_astronaut';
          break;
        case 3:
          iconCategory = 'space_travel';
          break;
        case 4:
          iconCategory = 'gravity_facts';
          break;
        case 5:
          iconCategory = 'walking_in_space';
          break;
        case 6:
          iconCategory = 'life_in_space';
          break;
        case 7:
          iconCategory = 'cool_facts';
          break;
      }
      iconUrl = 'https://s3.amazonaws.com/2018-google-space-stories/image/category_icon/' + iconCategory + '.png';
    } else {
      iconUrl = 'https://s3.amazonaws.com/2018-google-space-stories/image/category_icon/' + category + '.png';
    }

    console.log('url...', audioUrl);
    console.log('image url...', iconUrl);

     let messageIndex = Math.floor(Math.random()*(audioMsg.length - 1));
     let message = audioMsg[messageIndex];

    console.log('The audio sources is:' + audioUrl );
    let OutputUrl = `<audio src="${audioUrl}"/>` ;


    if(category === 'play_all'){

      for(let i=0; i < 6;i++){

        conv.user.storage.play_all_index += 1;
        audioUrl = 'https://s3.amazonaws.com/2018-google-space-stories/audio/' +
        category + '/' + conv.user.storage.play_all_index + '.mp3';
        OutputUrl =  OutputUrl + `<audio src="${audioUrl}"/>`;

      }

    }
     


     







    let speech = getSSMLSpeech(OutputUrl);

    
    

    if(category === 'play_all'){
      conv.ask(new SimpleResponse({
        speech: speech,
        text: message,
      }));



      // conv.user.storage.play_all_index += 1;
      // audioUrl = 'https://s3.amazonaws.com/2018-google-space-stories/audio/' +
      // category + '/' + conv.user.storage.play_all_index + '.mp3';
      // OutputUrl = `<audio src="${audioUrl}"/>` + '.';
      // speech = getSSMLSpeech(OutputUrl);
      // conv.ask(speech);




    
      

      



    conv.ask(new BasicCard({
      title: 'Mike Massimino',
      image: {
        url: iconUrl,
        accessibilityText: 'Mike Massimino',
      },
    }));
    conv.ask(new Suggestions('Next'));
  

    }else{

      conv.ask(new SimpleResponse({
        speech: speech,
        text: message,
      }));
    conv.ask(new BasicCard({
      title: 'Mike Massimino',
      image: {
        url: iconUrl,
        accessibilityText: 'Mike Massimino',
      },
    }));
    conv.ask(new Suggestions('Next'));

    


    }

    



  }








exports.handler = functions.https.onRequest(app);






// HELPER ======================================================================
function getSSMLSpeech(message) {
  let speech = '<speak>' + message + '</speak>';
  return speech;
}


//Below are the eight category


  
app.intent('History Of Space Intent', (conv) => {
  history_of_Space(conv);
  });

app.intent('Become An Astronaut', (conv) => {
  Become_An_Astronaut(conv);
  });

 app.intent('Space Travel Intent', (conv) => {
   Space_Travel(conv);
  });

 app.intent('Gravity Facts Intent', (conv) => {
  Gravity_Facts(conv);
 });

 app.intent('Walking In Space Intent', (conv) => {
  Walking_In_Space(conv);
 });

 app.intent('Life In Space Intent', (conv) => {
  Life_In_Space(conv);
 });


 app.intent('Cool Facts Intent', (conv) => {
  Cool_Facts(conv);
 });



function history_of_Space(conv){
  conv.user.storage.category = 'history_of_space';
  let category = conv.user.storage.category;
  let index = conv.user.storage.history_of_space_index;





  let message = categoryMsg;


  if (!index) {
    conv.user.storage.history_of_space_index = 1;
    index =conv.user.storage.history_of_space_index;
  }

  let audioTitle = 'History of Space';
  playAudio(category, audioTitle, index, conv);

}


function Become_An_Astronaut(conv){
  conv.user.storage.category = 'becoming_an_astronaut';
  let category = conv.user.storage.category;
  let index = conv.user.storage.becoming_an_astronaut_index;


  let message = categoryMsg;


  if (!index) {
    conv.user.storage.becoming_an_astronaut_index = 1;
    index =conv.user.storage.becoming_an_astronaut_index;
  }

  let audioTitle = 'Becoming an Astronaut';
  playAudio(category, audioTitle, index, conv);

}



function Space_Travel(conv){
  conv.user.storage.category = 'space_travel';
  let category = conv.user.storage.category;
  let index = conv.user.storage.space_travel_index;


  let message = categoryMsg;


  if (!index) {
    conv.user.storage.space_travel_index = 1;
    index =conv.user.storage.space_travel_index;
  }

  let audioTitle = 'Space Travel';
  playAudio(category, audioTitle, index, conv);

}



function Gravity_Facts(conv){
  conv.user.storage.category = 'gravity_facts';
  let category = conv.user.storage.category;
  let index = conv.user.storage.gravity_facts_index;


  let message = categoryMsg;


  if (!index) {
    conv.user.storage.gravity_facts_index = 1;
    index =conv.user.storage.gravity_facts_index;
  }

  let audioTitle = 'Gravity Facts';
  playAudio(category, audioTitle, index, conv);

}



function Walking_In_Space(conv){
  conv.user.storage.category = 'walking_in_space';
  let category = conv.user.storage.category;
  let index = conv.user.storage.walking_in_space_index;


  let message = categoryMsg;


  if (!index) {
    conv.user.storage.walking_in_space_index = 1;
    index =conv.user.storage.walking_in_space_index;
  }

  let audioTitle = 'Walking in Space';
  playAudio(category, audioTitle, index, conv);

}


function Life_In_Space(conv){
  conv.user.storage.category = 'life_in_space';
  let category = conv.user.storage.category;
  let index = conv.user.storage.life_in_space_index;


  let message = categoryMsg;


  if (!index) {
    conv.user.storage.life_in_space_index = 1;
    index =conv.user.storage.life_in_space_index;
  }

  let audioTitle = 'Life in Space';
  playAudio(category, audioTitle, index, conv);

}



function Cool_Facts(conv){
  conv.user.storage.category = 'cool_facts';
  let category = conv.user.storage.category;
  let index = conv.user.storage.cool_facts_index;


  let message = categoryMsg;


  if (!index) {
    conv.user.storage.cool_facts_index = 1;
    index =conv.user.storage.cool_facts_index;
  }

  let audioTitle = 'Cool Facts';
  playAudio(category, audioTitle, index, conv);

}



function Play_All(conv){
  conv.user.storage.category = 'play_all';
        let category =  conv.user.storage.category;
        let index =  conv.user.storage.play_all_index;  
  
        if (!index) {
          conv.user.storage.play_all_index = 1;
          index = conv.user.storage.play_all_index;
        }
        let audioTitle;

        switch (index) {
          case 1:
            audioTitle = 'History of Space';
            break;
          case 2:
            audioTitle = 'Becoming an Astronaut';
            break;
          case 3:
            audioTitle = 'Space Travel';
            break;
          case 4:
            audioTitle = 'Gravity Facts';
            break;
          case 5:
            audioTitle = 'Walking in Space';
            break;
          case 6:
            audioTitle = 'Life in Space';
            break;
          case 7:
            audioTitle = 'Cool Facts';
            break;
        }
        console.log('Everythings is good here');

        playAudio(category, audioTitle, index, conv);

}
